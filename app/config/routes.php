<?php

$routes = [
    'products' => 'Codeacademy\Products\Controller\Index',
    'categories' => 'Codeacademy\Categories\Controller\Index',
    'review' => 'Codeacademy\Reviews\Controller\Index',
];