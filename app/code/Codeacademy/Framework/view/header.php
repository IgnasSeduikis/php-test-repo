<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Codeacademy shopuks</title>
</head>
<body>
<header>
    <main>
        <div class="container">
            <nav class="">
                <ul>
                    <li>
                        <a href="edit">Edit Products</a>
                    </li>
                    <li>
                        <a href="create">Create new products</a>
                    </li>
                    <li>
                        <a href="categories/create">Create new Categories</a>
                    </li>
                    <li>
                        <a href="/">Home</a>
                    </li>
                </ul>
            </nav>
        </div>