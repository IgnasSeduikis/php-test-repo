<?php

namespace Codeacademy\Home\Controller;

use Codeacademy\Framework\Core\Controller;

class Index extends Controller
{

    public function __construct()
    {
        parent::__construct('Codeacademy/Products');
    }

    public function index()
    {
        echo 'Our home page';
        $this->render('This is something', 'something2');
    }
}