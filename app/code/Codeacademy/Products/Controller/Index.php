<?php

namespace Codeacademy\Products\Controller;

use \Codeacademy\Framework\Helper\FormBuilder;
use \Codeacademy\Framework\Helper\Request;
use \Codeacademy\Products\Model\Product;
use \Codeacademy\Framework\Helper\Url;
use Codeacademy\Framework\Core\Controller;



class Index extends Controller
{
    private $request;

    public function __construct()
    {
        $this->request= new Request();
        parent::__construct('Codeacademy/Products');
    }


    public function index()
    {
        $product = new Product();
        // $product->load($id);
        // print_r($product);
    }



    public function delete($id)
    {
        $id = (int) $id;
        $product = new Product();
        $product->delete($id);

    }

    public function import(){
        $url = 'https://manopora.lt/ca/spc.xml';
        $path = PROJECT_ROOT_DIR.'var/import/';
        $fileName = 'products'.date('d-m-Y').'.xml';
        $file = $path.$fileName;
        $fp=fopen($file,'w+');

        $curlHandler = curl_init($url);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);
        echo '<pre>';

        $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
        foreach ($xml->produkt as $product)
        {
            $sku = (string)$product->kzs;
            if ($this->checkSkuUniq($sku)){
                continue;
            }
            $name = (string)$product->nazwa;
            $qty = (int)$product->status;
            $price = (float)$product->cena_zewnetrzna;
            $description = (string)$product->dlugi_opis;

            $newProduct = new Product();
            $newProduct->setSku($sku);
            $newProduct->setName($name);
            $newProduct->setQty($qty);
            $newProduct->setPrice($price);
//            $newProduct->setDesc($description);

            $newProduct->save();

        }

    }

    public function stockUpdate()
    {
        $url = 'https://manopora.lt/ca/stocks.csv';
        $path = PROJECT_ROOT_DIR.'var/import/';
        $fileName = 'stocks'.date('d-m-Y').'.csv';
        $file = $path.$fileName;
        $fp=fopen($file,'w+');

        $curlHandler = curl_init($url);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);

        $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);

    }

    public function showFile()
    {

    }

    public function checkSkuUniq($sku)
    {
//        $db = new SqlBuilder;
//        $allSku = $db->select('sku')->from('products')->getAll();
//
//        $allSku = array_merge_recursive(...array_values($allSku))["sku"];
//
//        if(in_array($sku, $allSku) || $sku === "" || $sku === Null) {
//            echo "Product with SKU: " . $sku . " already exists or SKU input was empty.<br>";
//            return false;
//        } else {
//            echo "Product with SKU: " . $sku . " is unique.<br>";
            return true;
        }

    public function edit($id)
    {
        $id = (int) $id;
        $products = new Product();
        $product = $products->load('id',$id);

        $formHelper = new FormBuilder('POST', '/products/update', 'form', 'product-form');
        $formHelper->input('text','name', 'form-control', 'product-name', 'Product Name', $product->getName());
        $formHelper->input('text','description', 'form-control', 'product-description', 'Product Description', $product->getDesc());
        $formHelper->input('text','sku', 'form-control', 'product-sku', 'Product Sku', $product->getsku());
        $formHelper->input('text','price', 'form-control', 'product-price', 'Product Price', $product->getPrice());
        $formHelper->input('text','special_price', 'form-control', 'product-special-price', 'Product Special Price', $product->getSpecialPrice());
        $formHelper->input('text','cost', 'form-control', 'product-cost', 'Product Cost', $product->getCost());
        $formHelper->input('number','qty', 'form-control', 'product-qty', 'qty', $product->getQty());
        $formHelper->input('hidden','id', 'form-control', 'product-id', 'id',$id);

        $formHelper->button('ok', 'Edit','btn btn-info mb-3');
        echo $formHelper->get();

    }

    public function create()
    {

        $formHelper = new FormBuilder('POST', Url::getUrl('products/store'), 'form', 'product-form');
        $formHelper->input('text','name', 'form-control', 'product-name', 'Product Name');
        $formHelper->input('text','description', 'form-control', 'product-description', 'Product Description');
        $formHelper->input('text','sku', 'form-control', 'product-sku', 'Product Sku');
        $formHelper->input('text','price', 'form-control', 'product-price', 'Product Price');
        $formHelper->input('text','special_price', 'form-control', 'product-special-price', 'Product Special Price');
        $formHelper->input('text','cost', 'form-control', 'product-cost', 'Product Cost');
        $formHelper->input('number','qty', 'form-control', 'product-qty', 'qty');
        $formHelper->button('ok', 'Create','btn btn-info mb-3');

        $data['title'] = 'Add New Product';
        $data['form'] = $formHelper->get();
        $this->render('form/create', $data);
        // WEB temnpalte + forma;
    }
// POST requestus
    public function store()
    {
        $name  = $this->request->getPost('name');
        $sku  = $this->request->getPost('sku');
        $price  = $this->request->getPost('price');
        $qty  = $this->request->getPost('qty');
        $product = new Product();
        $product->setName($name);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setQty($qty);
        $product->save();

    }

    public function update($id)
    {
        $id = $this->request->getPost('id');
        $name  = $this->request->getPost('name');
        $description  = $this->request->getPost('description');
        $sku  = $this->request->getPost('sku');
        $price  = $this->request->getPost('price');
        $special_price  = $this->request->getPost('special_price');
        $cost  = $this->request->getPost('cost');
        $qty  = $this->request->getPost('qty');
        $product = new Product();
        $product->load('id',$id);

        $product->setName($name);
        $product->setDesc($description);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setSpecialPrice($special_price);
        $product->setCost($cost);
        $product->setQty($qty);
        $product->save();

    }
}
