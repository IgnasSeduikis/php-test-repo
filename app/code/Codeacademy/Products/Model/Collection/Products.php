<?php

namespace Codeacademy\Products\Model\Collection;

use Codeacademy\Framework\Helper\SqlBuilder;
use Codeacademy\Products\Model\Product;

class Products
{
    private $collection = [];

    public function __construct()
    {
        $this->initCollection();
        return $this;
    }

    //move to sqlbuilder
    private function cleanResults($result)
    {
        $cleanArray = [];
        foreach ($result as $element) {
            foreach ($element as $line) {
                $cleanArray[] = $line;
            }
        }
        return $cleanArray;
    }

    public function addCategoryFilter($id)
    {
// select productid from productscategories where categoryid = $id
        $db = new SqlBuilder();
        $productIds = $db->select('product_id')->from('category_related_products')->where('category_id', $id)->get();
        $productIds = $this->cleanResults($productIds);
        print_r($productIds);
        foreach ($this->collection as $product) {
            if (!in_array($product->getId(), $productIds)) {
                unset($this->collection[$product->getId()]);
            }
        }
    }

    public function addFilter($filter, $value, $operator)
    {
    }



    public function getCollection()
    {
        return $this->collection;
    }

    public function initCollection()
    {
        $db = new SqlBuilder();
        $productsIds = $db->select('id')->from('products')->get(); //->where() <- jei neveiks
        echo "<pre>";
        foreach ($productsIds as $element) {
            $product = new Product();
            $this->collection[$element['id']] = $product->load('id', $element['id']);
        }
    }
}