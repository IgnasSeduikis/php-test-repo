<?php

namespace Codeacademy\Products\Model;

use \Codeacademy\Framework\Helper\SqlBuilder;

class Product
{
    private $id;
    private $name;
    private $sku;
    private $price;
    private $qty;

    public function __construct($id = '')
    {
        if ($id !== '') {
            $this->load($id);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param mixed $qty
     */
    public function setQty($qty): void
    {
        $this->qty = $qty;
    }

    public function load($field, $value, $selection = '*')
    {
        $db = new SqlBuilder();
        // print_r($db->select($selection)->from('products')->where($field, $value)->getOne());
        // die();
        $product = $db->select($selection)->from('products')->where($field, $value)->getOne();


        $this->id = $product['id'] ?? '';
        $this->name = $product['name'] ?? '';
        $this->description = $product['description'] ?? '';
        $this->sku = $product['sku'] ?? '';
        $this->price = $product['price'] ?? '';
        $this->special_price = $product['special_price'] ?? '';
        $this->cost = $product['cost'] ?? '';
        $this->qty = $product['qty'] ?? '';

        return $this;
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
            echo "update";
        } else {
            $this->create();
            echo "create";
        }

    }

    private function update()
    {
        $db = new SqlBuilder();

        $product = [
            'name' => $this->name,
            'sku' => $this->sku,
            'price' => $this->price,
            'qty' => $this->qty,
        ];

        $db->update('products')->set($product)->where('id', $this->id)->exec();
    }

    private function create()
    {
        $product = [
            'name' => $this->name,
            'sku' => $this->sku,
            'price' => $this->price,
            'qty' => $this->qty,
        ];

        $db = new SqlBuilder();
        $db->insert('products')->values($product)->exec();
    }

    public function delete($id)
    {
        $db = new SqlBuilder();
        $db->delete('products')->where('id', $id)->exec();
    }

    public function checkSkuUnix()
    {
        //
    }

}


