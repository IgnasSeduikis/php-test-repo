<?php

namespace Codeacademy\Categories\Controller;

use \Codeacademy\Framework\Helper\FormBuilder;
use \Codeacademy\Framework\Helper\Request;
use Codeacademy\Products\Model\Collection\Products;
use \Codeacademy\Products\Model\Product;

class Index
{
    public function index()
    {
        echo 'Not found from controller';
    }

    public function show($id)
    {
        echo 'Show category';
        $productsModel = new Products();
        $productsModel->addCategoryFilter($id);
        $products = $productsModel->getCollection();

        print_r($products);
    }
}